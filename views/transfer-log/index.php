<?php

use app\models\Quota;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transfer Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transfer-log-index">

    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'user_id' => [
                'class' => DataColumn::className(),
                'attribute' => 'user_id',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->user->name;
                }
            ],
            'company_id' => [
                'class' => DataColumn::className(),
                'attribute' => 'company_id',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->company->name;
                }
            ],
            'create_date' => [
                'class' => DataColumn::className(),
                'attribute' => 'create_date',
                'format' => 'html',
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->create_date);
                }

            ],
            'transferred'=> [
                'class' => DataColumn::className(),
                'attribute' => 'transferred',
                'format' => 'html',
                'value' => function ($model) {
                    $qArr = Quota::getBigValue($model->transferred);
                    return $qArr[0].' '.Quota::$name[$qArr[1]];
                }
            ],
            'resourced',

            [
                'class' => \yii\grid\ActionColumn::className(),
                'template' => '{delete}',
            ]
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
