<?php

use app\models\Quota;
use yii\bootstrap\Modal;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Company', '', ['class' => 'btn btn-success add-company' ]) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'quota'=> [
                'class' => DataColumn::className(),
                'attribute' => 'quota',
                'format' => 'html',
                'value' => function ($model) {
                    $qArr = Quota::getBigValue($model->quota);
                    return $qArr[0].' '.Quota::$name[$qArr[1]];
                }
            ],
            [
                'class' => \yii\grid\ActionColumn::className(),
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return '<a href="" onclick="idCompany='.$model->company_id.'" class="update-company" title="Update"><span class="glyphicon glyphicon-pencil"></span></a>';
                    },



                ],
                'template' => '{update}  {delete}',
            ]
        ],
    ]); ?>
<?php Pjax::end();
Modal::begin([
    'header' => '<h2>Create company</h2>',
    'id'=>"modalCompany"
]);?>
    <div class="modal-content">
        <div class="modal-body">
            ...
        </div>
    </div>

<?php Modal::end();
?></div>
<script type="application/javascript">
    var idCompany = 0;
    window.addEventListener('load', function () {
        $('.add-company, .update-company').click(function(event){
            event.preventDefault();
            var url = '/company/form?id='+idCompany;
            var modalContainer = $('#modalCompany');
            $.ajax({
                url: url,
                type: "GET",
                data: {},
                success: function (data) {
                    $('.modal-body').html(data);
                    if(idCompany>0){
                        $('.modal-header h2').text('Update company');
                    }

                    modalContainer.modal({show:true});
                }
            });
        });
        $(document).on("submit", '.company-form', function (e) {
            e.preventDefault();
            var form = $(this);
            var url = "/company/create";
            if(idCompany>0){
                url = '/company/update?id='+idCompany;
            }

            $.ajax({
                url: url,
                type: "POST",
                data: form.serialize(),
                success: function (result) {
                    console.log(result);
                    var modalContainer = $('#modalCompany');
                    var modalBody = modalContainer.find('.modal-body');
                    if (result == 'true') {
                        modalBody.html("<div class='alert alert-success'>");
                        $('.alert-success').append("<strong>Success!</strong>");
                        $('.alert-success').append('</div>');
                        setTimeout(function() {
                            $("#modalCompany").modal('hide');
                        }, 4000);
                        window.location.reload(false);

                    }
                    else {
                        modalBody.html(result).hide().fadeIn();
                    }
                }
            });
            return false;
        });
    }, false);
</script>
