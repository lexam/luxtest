<?php

use app\models\Quota;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'name',
            'value' => function ($model) {
                return $model['name'];
            }

        ],
//        [
//            'attribute' => 'date',
//            'value' => function ($model) {
//                return date('M',$model['create_date']);
//            }
//
//        ],
        'transferred'=> [
            'class' => DataColumn::className(),
            'attribute' => 'used',
            'format' => 'html',
            'value' => function ($model) {
                $qArr = Quota::getBigValue($model['transferred']);
                return $qArr[0].' '.Quota::$name[$qArr[1]];
            }
        ],
        'quota'=> [
            'class' => DataColumn::className(),
            'attribute' => 'quota',
            'format' => 'html',
            'value' => function ($model) {
                $qArr = Quota::getBigValue($model['quota']);
                return $qArr[0].' '.Quota::$name[$qArr[1]];
            }
        ],

        ],
    ]); ?>
<?php Pjax::end(); ?></div>
