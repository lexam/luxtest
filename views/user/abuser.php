<?php

use yii\bootstrap\Modal;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\models\User */

$this->title = 'AbUsers';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <input type='text' id='txtDate' />
        <?= Html::a('Show report', '', ['class' => 'btn btn-success show-report']) ?>
        <?= Html::a('Generate date', '', ['class' => 'btn btn-success generate-trafic']) ?>
        <div class="loader" style="display: none"></div>

    </p>
    <div class="report-view">
    </div>
</div>
<script type="application/javascript">
    window.addEventListener('load', function () {

        $('.generate-trafic').click(function(event){
            event.preventDefault();
            $('.loader').show();
            $(this).hide();
            var url = '/user/generate';
            $.ajax({
                url: url,
                type: "GET",
                data: {},
                success: function (data) {
                    $('.loader').hide();
                    $('.generate-trafic').show();
                }
            });
        });
        $('.show-report').click(function(event){
            event.preventDefault();
            var d = $('#txtDate').val();
            if( d==''){
                alert('Choice date!');
                return false;
            }

            var url = '/user/report?d='+d;
            $.ajax({
                url: url,
                type: "GET",
                data: {},
                success: function (data) {

                    $('.report-view').html(data);

                }
            });
        });
        $('#txtDate').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'MM yy',

            onClose: function() {
                var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
            },

            beforeShow: function() {
                if ((selDate = $(this).val()).length > 0)
                {
                    iYear = selDate.substring(selDate.length - 4, selDate.length);
                    iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
                    $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
                    $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
                }
            }
        });

    }, false);
</script>