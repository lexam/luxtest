<?php

use yii\bootstrap\Modal;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\models\User */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User', '', ['class' => 'btn btn-success add-user', 'onclick'=>"idUser=0"]) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'email:email',
            'company_id'=> [
                'class' => DataColumn::className(),
                'attribute' => 'company_id',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->company->name;
                }
            ],
            [
                'class' => \yii\grid\ActionColumn::className(),
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return '<a href="" onclick="idUser='.$model->user_id.'" class="update-user" title="Update"><span class="glyphicon glyphicon-pencil"></span></a>';
                    },
                ],
                'template' => '{update}  {delete}',
            ]
        ],
    ]); ?>
<?php Pjax::end();
Modal::begin([
    'header' => '<h2>Create user</h2>',
    'id'=>"modalUser"
]);?>
<div class="modal-content">
    <div class="modal-body">
        ...
    </div>
</div>

<?php Modal::end();
?></div>
<script type="application/javascript">
    var idUser = 0;
    window.addEventListener('load', function () {
        $('.add-user, .update-user').click(function(event){
            event.preventDefault();
            var url = '/user/form?id='+idUser;
            var modalContainer = $('#modalUser');
            $.ajax({
                url: url,
                type: "GET",
                data: {},
                success: function (data) {
                    $('.modal-body').html(data);
                    if(idUser>0){
                        $('.modal-header h2').text('Update user');
                    }

                    modalContainer.modal({show:true});
                }
            });
        });
        $(document).on("submit", '.user-form', function (e) {
            e.preventDefault();
            var form = $(this);
            var url = "/user/create";
            if(idUser>0){
                url = '/user/update?id='+idUser;
            }

            $.ajax({
                url: url,
                type: "POST",
                data: form.serialize(),
                success: function (result) {
                    var modalContainer = $('#modalUser');
                    var modalBody = modalContainer.find('.modal-body');
                    if (result == 'true') {
                        modalBody.html("<div class='alert alert-success'>");
                        $('.alert-success').append("<strong>Success!</strong>");
                        $('.alert-success').append('</div>');
                        setTimeout(function() {
                            $("#modalUser").modal('hide');
                        }, 4000);
                        window.location.reload(false);

                    }
                    else {
                        modalBody.html(result).hide().fadeIn();
                    }
                }
            });
            return false;
        });
    }, false);
</script>