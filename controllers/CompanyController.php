<?php

namespace app\controllers;

use app\models\Quota;
use app\models\TransferLog;
use app\models\User;
use Yii;
use app\models\Company;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Company::find(),
        ]);
        $model = new Company();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }


    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Company();
        if($model->load(Yii::$app->request->post())) {
            $model->quota = Quota::getSmallValue($model->quota, $model->quotaName);
            Yii::trace($model->quota);
            if($model->save()){
                $success=true;
                return json_encode($success);
            }

        }
        return $this->renderPartial('_form', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if($model->load(Yii::$app->request->post())) {
            $model->quota = Quota::getSmallValue($model->quota, $model->quotaName);
            if($model->save()){
                $success=true;
                return json_encode($success);
            }
        }
        $qArr = Quota::getBigValue($model->quota);
        $model->quota = $qArr[0];
        $model->quotaName = $qArr[1];
        return $this->renderPartial('_form', [
            'model' => $model,
        ]);

    }


    /**
     * Render form
     * @return string
     */
    public function actionForm($id=0)
    {
        if($id>0){
            $model = $this->findModel($id);
            $qArr = Quota::getBigValue($model->quota);
            $model->quota = $qArr[0];
            $model->quotaName = $qArr[1];
        }else{
            $model = new Company();
        }


        return $this->renderPartial('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        TransferLog::deleteAll(['company_id'=>$id]);
        User::deleteAll(['company_id'=>$id]);
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
