<?php

namespace app\controllers;

use app\models\TransferLog;
use Yii;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $success = true;
            return json_encode($success);
        } else {
            return $this->renderPartial('_form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $success = true;
            return json_encode($success);
        } else {
            return $this->renderPartial('_form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Render form
     * @return string
     */
    public function actionForm($id = 0)
    {
        if ($id > 0) {
            $model = $this->findModel($id);
        } else {
            $model = new User();
        }


        return $this->renderPartial('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        TransferLog::deleteAll(['user_id' => $id]);
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return string
     */
    public function actionAbuser()
    {

        return $this->render('abuser', [

        ]);
    }

    /**
     * @return string
     */
    public function actionReport($d)
    {
        $d = date('m',strtotime($d));
        $dataProvider = new ArrayDataProvider([
            'allModels' => TransferLog::findBySql('SELECT * FROM (SELECT `transferred`, `name`, `quota`, create_date FROM `transfer_log` 
                                                   LEFT JOIN `company` ON transfer_log.company_id=company.company_id WHERE MONTH(FROM_UNIXTIME(`create_date`))='.$d.'
                                                   GROUP BY MONTH(FROM_UNIXTIME(`create_date`)), transfer_log.`company_id`) s 
                                                   WHERE s.transferred > s.quota ORDER BY s.transferred DESC')->asArray()->all(),
            'sort' => false

        ]);
        return $this->renderPartial('grid-transfer', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionGenerate()
    {
        $time = strtotime(date('Y-m-d'));
        $allUser = User::find()->select('user_id, company_id')->asArray()->all();
        $maxUser = count($allUser) - 1;
        $lastTime = $time-3600*24*180;
        $f = true;
        do {
            $count = rand(3, 6);
            for ($i = 0; $i < $count; $i++) {
                $u = $allUser[rand(0, $maxUser)];
                $t = new TransferLog();
                $rb = rand(100, 10995116277760);
                $t->company_id = $u['company_id'];
                $t->user_id = $u['user_id'];
                $t->create_date = rand($time, ($time + 3600 * 23));
                $t->transferred = $rb;
                $t->resourced = 'http://test.com/' . md5($rb);
                $t->save();
            }
            $time -= 3600 * 24;
            if ($time < $lastTime) {
                $f = false;
            }
        } while ($f);
        $success = true;
        return json_encode($success);
    }
}
