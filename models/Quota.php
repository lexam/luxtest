<?php

namespace app\models;

use yii\base\Model;

/**
 * This is the model class for calculating and view quota.
 *

 */
class Quota extends Model
{
    public static $name=['B', 'KB', 'MB', 'GB', 'TB'];

    /**
     * @param $bytes
     * @param int $precision
     * @return string
     */
    public static function getBigValue($bytes, $precision = 2)
    {
        $bytes = max($bytes, 0);
        $pow = floor(($bytes?log($bytes):0)/log(1024));
        $pow = min($pow, count(self::$name)-1);
        $bytes /= pow(1024, $pow);
        return [round($bytes, $precision), $pow];
    }

    /**
     * @param $size
     * @param $n
     * @return int
     */
    public static function getSmallValue($size, $n)
    {
        switch($n)  {
            case 4: $size *= 1024;
            case 3: $size *= 1024;
            case 2: $size *= 1024;
            case 1: $size *= 1024;
        }
        return $size;
    }
}
