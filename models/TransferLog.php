<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transfer_log".
 *
 * @property integer $transfer_log_id
 * @property integer $user_id
 * @property integer $company_id
 * @property integer $create_date
 * @property string $transferred
 * @property string $resourced
 *
 * @property User $user
 * @property Company $company
 */
class TransferLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transfer_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'company_id', 'create_date', 'transferred', 'resourced'], 'required'],
            [['user_id', 'company_id', 'create_date', 'transferred'], 'integer'],
            [['resourced'], 'string', 'max' => 150],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'company_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'transfer_log_id' => 'Transfer Log ID',
            'user_id' => 'User ID',
            'company_id' => 'Company ID',
            'create_date' => 'Create Date',
            'transferred' => 'Transferred',
            'resourced' => 'Resourced',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['company_id' => 'company_id']);
    }
}
