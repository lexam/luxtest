<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "company".
 *
 * @property integer $company_id
 * @property string $name
 * @property integer $quota
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $quotaName = 0;
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'quota', 'quotaName'], 'required'],
            [['quota', 'quotaName'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'name' => 'Name',
            'quota' => 'Quota',
            'quotaName' => 'Type',
        ];
    }

    /**
     * Get all company
     * @return array
     */
    public static function getAllCompany()
    {
        return ArrayHelper::map(self::find()->select(['company_id', 'name'])->asArray()->all(), 'company_id', 'name');
    }
}
